package com.ml4ai.junit;

import com.google.gson.Gson;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class Nd4jTester {

    @Test
    public void testRand_normal_distribution() {
        Gson gson = new Gson();
        INDArray nd_array = Nd4j.randn(Nd4j.create(new int[]{5, 5}));
        INDArray rand_ = Nd4j.rand(new int[]{1, 2, 3});
        System.out.println(gson.toJson(nd_array.data().asDouble()));
        System.out.println(gson.toJson(rand_.data().asDouble()));
    }

}
